package activity5;

public class Basketball implements Projectile{

    public void beTossed(Player ply, Target tar)
    {
        double playerPos = ply.getPosition();
        double targetPos = tar.getPosition();
        double distance = Math.abs(playerPos - targetPos);
        if (distance <=5)
        {
            tar.receive(this);
        }
        else
        {
            System.out.println("Missed the target.");
        }
    }
}
