package activity5;

public interface Projectile {
    void beTossed(Player ply, Target tar);
}
