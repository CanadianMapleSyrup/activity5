package activity5;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;

public class InteractionsTest {
    @Test
    public void giveProjectileTest()
    {
        Projectile ball = new Basketball();
        Player p = new Player("Steve",10);
        assertTrue(p.giveProjectile(ball));
    }

    @Test
    public void alreadyHasProjectileTest()
    {
        Projectile ball = new Basketball();
        Player p = new Player("Steve",10);
        p.giveProjectile(ball);
        assertFalse(p.giveProjectile(ball));
    }

    @Test
    public void noProjectileTossTest()
    {
        Target tar = new Hoop(10);
        Player p = new Player("Steve",10);
        assertFalse(p.toss(tar));
    }

    @Test
    public void hasProjectileTossTest()
    {
        Projectile ball = new Basketball();
        Target tar = new Hoop(10);
        Player p = new Player("Steve",10);
        p.giveProjectile(ball);
        assertTrue(p.toss(tar));
    }

    @Test
    public void receivePassTest()
    {
        Projectile ball = new Basketball();
        Player p = new Player("Steve",10);
        Player p2 = new Player("Ann",5);
        p.giveProjectile(ball);
        p.toss(p2);
        assertEquals(ball, p2.getProjectile());
    }
}
