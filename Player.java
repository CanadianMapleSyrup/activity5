package activity5;

public class Player implements Target{
    private String name;
    private double position;
    private Projectile heldProjectile;

    public Player(String name, double position)
    {
        this.name = name;
        this.position = position;
    }

    public boolean giveProjectile(Projectile prj)
    {
        if (this.heldProjectile == null)
        {
            this.heldProjectile = prj;
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean toss(Target tar)
    {
        if (this.heldProjectile == null)
        {
            return false;
        }
        else
        {
            this.heldProjectile.beTossed(this, tar);
            return true;
        }
        
    }

    public void receive(Projectile prj)
    {
        if (this.heldProjectile == null)
        {
            this.heldProjectile = prj;
            System.out.println(this.name + " Great Catch!");
        }
        else
        {
            System.out.println(this.name + " could not catch the ball.");
        }
    }

    public String getName()
    {
        return this.name;
    }

    public double getPosition()
    {
        return this.position;
    }

    public Projectile getProjectile()
    {
        return this.heldProjectile;
    }
}
