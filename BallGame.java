package activity5;

public class BallGame {
    public static void main(String[] args) {
        play2Basketball();
    }

    public static void playBasketball()
    {
        Player player = new Player("Ann", 0);
        Basketball ball = new Basketball();
        Hoop hoop = new Hoop(3);

        player.giveProjectile(ball);
        player.toss(hoop);
    }

    public static void play2Basketball()
    {
        Player ann = new Player("Ann", 0);
        Player bob = new Player("Bob", 4);
        Basketball ball = new Basketball();
        Hoop hoop = new Hoop(8);
        ann.giveProjectile(ball);
        ann.toss(hoop);
        ann.toss(bob);
        bob.toss(hoop);
    }
}
