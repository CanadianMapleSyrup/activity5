package activity5;

public class BocceBall implements Projectile, Target{
    
    double position;

    public void beTossed(Player ply, Target tar)
    {
        double playerPos = ply.getPosition();
        double targetPos = tar.getPosition();
        double distance = Math.abs(playerPos - targetPos);
        if (distance <= 3)
        {
            tar.receive(this);
            this.position = targetPos;
            System.out.println("Hit the Bocce ball");
        }
        else
        {
            System.out.println("Missed the target.");
            if (playerPos < targetPos)
            {
                this.position = playerPos + 3;
            }
            else
            {
                this.position = playerPos - 3;
            }
        }
    }

    public void receive(Projectile prj)
    {
        if (prj instanceof BocceBall)
        {
            System.out.println("Bocce ball was hit!");
            this.position += 0.5;
        }
        else
        {
            System.out.println("That was not a Bocce ball.");
        }
    }

    public double getPosition()
    {
        return this.position;
    }
}